from django.conf import settings
from django.urls import include, path
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views

from rest_framework import routers#, serializers, viewsets
from users.apiv1 import UserViewSet
from market.apiv1 import PostingPublicMarketViewSet, FreeTermPublicMarketSerializer, TransactionPublicMarketViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'postings', PostingPublicMarketViewSet)
router.register(r'free_terms', FreeTermPublicMarketSerializer)
router.register(r'public_transactions', TransactionPublicMarketViewSet)

urlpatterns = [
    path("", TemplateView.as_view(template_name="pages/home.html"), name="home"),
    path("about/", TemplateView.as_view(template_name="pages/about.html"), name="about"),
    path('grappelli/', include('grappelli.urls')), # grappelli URLS
    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),
    # User management
    path("users/", include("mevvy.users.urls", namespace="users")),
    path("accounts/", include("allauth.urls")),
    # API v1 Endpoints (Django Rest Framework)
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # Your stuff: custom urls includes go here
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
