from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline, GenericStackedInline
from .models import NegotiableTerm, FreeTerm, Posting, Transaction
from mevvy.financial.models import DoubleEntryAccount

@admin.register(NegotiableTerm)
class NegotiableTermAdmin(admin.ModelAdmin):
    list_display = ('content_object', 'owner', 'receiver', 'fulfiller',)
    #list_display_links = ('text',)
    #readonly_fields = ['created_at', 'updated_at',]
    search_fields = ['owner', 'receiver', 'fulfiller', "content_object"]
    save_on_top = True
    autocomplete_lookup_fields = {
        'generic': [['content_type', 'object_id'],],
        'fk': ['owner','receiver','fulfiller'],
    }

class FreeTermInline(GenericStackedInline):
    model = FreeTerm
    extra = 0
    readonly_fields = ('other_party_accepted_negotiation','other_party_declined_negotiation',
        'other_party_accepted_fulfillment','other_party_declined_fulfillment',)
    autocomplete_lookup_fields = {
        'fk': ['owner','receiver','fulfiller'],
    }
    classes = ('grp-collapse grp-open',)
    inline_classes = ('grp-collapse grp-open',)



@admin.register(Posting)
class PostingAdmin(admin.ModelAdmin):
    list_display = ('title', 'type', 'owner', 'created_at', 'updated_at')
    list_display_links = ('title',)
    readonly_fields = ['created_at', 'updated_at',]
    search_fields = ["owner","title","description"]
    filter_fields = ['type',]
    save_on_top = True
    inlines = [FreeTermInline,]

"""
class TransactionAccountInline(GenericTabularInline):
    model = DoubleEntryAccount
    ct_fk_field = "object_id"
    ct_field = "content_type"
    extra = 0
    can_delete = False
    #readonly_fields = ('money_saldo','merit_saldo')
"""

@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'party_1', 'party_2', 'status', 'public')
    list_display_links = ('__str__',)
    readonly_fields = ['created_at', 'updated_at']
    search_fields = ['party_1', 'party_2',]
    save_on_top = True
    inlines = [FreeTermInline]
    autocomplete_lookup_fields = {
        #'generic': [['content_type', 'object_id'],],
        #'fk': ['party_1','party_2','posting'],
    }
