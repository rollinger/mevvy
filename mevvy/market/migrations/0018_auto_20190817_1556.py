# Generated by Django 2.2.3 on 2019-08-17 14:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('market', '0017_posting'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='request',
            name='owner',
        ),
        migrations.AlterField(
            model_name='transaction',
            name='content_type',
            field=models.ForeignKey(blank=True, limit_choices_to=models.Q(models.Q(('app_label', 'market'), ('model', 'posting')), models.Q(('app_label', 'market'), ('model', 'offer')), models.Q(('app_label', 'market'), ('model', 'request')), _connector='OR'), null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType'),
        ),
        migrations.DeleteModel(
            name='Offer',
        ),
        migrations.DeleteModel(
            name='Request',
        ),
    ]
