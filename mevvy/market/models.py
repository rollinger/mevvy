from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from django.db.models.signals import post_save
from django.db.models import Q
from django.dispatch import receiver

from mevvy.financial.models import MoneyAccount, MeritAccount

User = get_user_model()



NEGOTIATION_STATUS = (
    (0,_('NEGOTIATION OPEN')),
    (1,_('NEGOTIATION COMPLETED')),
    (2,_('NEGOTIATION ABORTED')),
    (3,_('FULFILLMENT OPEN')),
    (4,_('FULFILLMENT COMPLETED')),
    (5,_('FULFILLMENT ABORTED')),
)
class NegotiableTerm(models.Model):
    """
    The base class handles status and process of Negotiable Terms (like FreeTerm, Products, Services, etc...)
    """

    # The model the Negotiable Term belongs to
    limit = models.Q( app_label = 'market', model = 'posting') | \
        models.Q(app_label = 'market', model = 'transaction')
    content_type = models.ForeignKey(ContentType, limit_choices_to = limit, on_delete=models.CASCADE,)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    # Only the creator can amend the term
    owner       = models.ForeignKey(User, related_name='owned_terms', on_delete=models.CASCADE, null=True, blank=True)
    receiver    = models.ForeignKey(User, related_name='receiving_terms', on_delete=models.CASCADE, null=True, blank=True)
    fulfiller   = models.ForeignKey(User, related_name='fulfilling_terms', on_delete=models.CASCADE, null=True, blank=True)

    # Status Flags
    _negotiable     = models.BooleanField(_("This item can be negotiated"), default=True)
    status          = models.PositiveSmallIntegerField(_('Status of the Negotiation'), choices=NEGOTIATION_STATUS, default=0)

    other_party_accepted_negotiation = models.DateTimeField(_('Receiving party accepted negotiation on'), null=True, blank=True)
    other_party_declined_negotiation = models.DateTimeField(_('Receiving party declined negotiation on'), null=True, blank=True)
    other_party_accepted_fulfillment = models.DateTimeField(_('Receiving party accepted fulfillment on'), null=True, blank=True)
    other_party_declined_fulfillment = models.DateTimeField(_('Receiving party declined fulfillment on'), null=True, blank=True)

    def receiving_party(self):
        # The party that receive the fulfillment of the term
        return self.receiver

    def fulfilling_party(self):
        # The party that has to fulfill the term to the receiving party
        return self.fulfiller

    def not_owner(self):
        # Returns the party that is not the owner of this term
        if self.owner == self.receiver:
            return self.fulfiller
        return receiver

    def accept_negotiation(self, user):
        if _negotiable and _status == 0 and user == not_owner():
            self.not_owner_accepted_negotiation = timezone.now()
            self.status = 1
            self.save()

    def decline_negotiation(self, user):
        if _negotiable and _status == 0 and user == not_owner():
            self.not_owner_declined_negotiation = timezone.now()
            self.status = 2
            self.save()

    def accept_fulfillment(self, user):
        if _status == 3 and user == receiving_party():
            self.receiver_accepted_fulfillment = timezone.now()
            self.status = 4
            self.save()

    def decline_fulfillment(self, user):
        if _status == 3 and user == receiving_party():
            self.receiver_decline_fulfillment = timezone.now()
            self.status = 5
            self.save()

    def negotiable(self):
        if self._negotiable:
            return _('(Negotiable)')
        return _('(Not Negotiable)')

    def __str__(self):
        return super(NegotiableTerm, self).__str__() #+ self.negotiable()

    def save(self, *args, **kwargs):
        # When creating a non negotiable item advance the status to fulfillment open
        if not self.pk and not self._negotiable:
            self.status = 3
        # If any element is changed issue a log entry about that change
        # TODO: Use the Log Facility of Django
        super(NegotiableTerm, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('Negotiable Term')
        verbose_name_plural = _('Negotiable Terms')


class FreeTermManager(models.Manager):
    pass
class FreeTerm(NegotiableTerm, models.Model):
    """
    FreeTerm is a Negotiable Free Text Element for Transactions or Offers/Requests
    """

    text = models.CharField(_("Text of the term"), max_length=500)

    # Internal fields
    created_at  = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated_at  = models.DateTimeField(_('Updated at'), auto_now=True)

    objects = FreeTermManager()

    def __str__(self):
        return "%s fulfills term for %s" % (self.fulfiller, self.receiver)

    class Meta:
        verbose_name = _('Free Term')
        verbose_name_plural = _('Free Terms')
        ordering = ['-created_at',]



POSTING_TYPE = (
    (0,_('Offer')),
    (1,_('Request')),
)
class PostingManager(models.Manager):
    def all_published(self):
        postings = self.filter(published=True)
        return postings
class Posting(models.Model):

    owner       = models.ForeignKey(User, on_delete=models.PROTECT)

    type        = models.PositiveSmallIntegerField(_('Type of the Posting'), choices=POSTING_TYPE, default=0)

    title       = models.CharField(_("Title of the posting"), max_length=255)
    description = models.TextField(_("Description of the posting"), max_length=2500, blank=True, null=True)

    # Flags
    published   = models.BooleanField(_("Published"), default=True)

    # Terms
    free_terms  = GenericRelation('FreeTerm')
    # TODO: More type of terms

    # Internal fields
    created_at  = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated_at  = models.DateTimeField(_('Updated at'), auto_now=True)

    objects = PostingManager()

    def __str__(self):
        return _("%s: %s ( %s )") % (self.get_type_display(), self.title, self.owner)

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "title__icontains", "owner__name__icontains" )

    class Meta:
        verbose_name = _('Posting')
        verbose_name_plural = _('Postings')
        ordering = ['-created_at',]



TRANSACTION_STATUS = (
    (1,_('STARTED')),       #
    (5,_('NEGOTIATION COMPLETED')),
    (10,_('TRANSACTION FULFILLED')),    #
    (15,_('MERIT DONATION CLOSED')),       #
    (20,_('TRANSACTION COMLETED')),
)
class TransactionManager(models.Manager):
    def all_public(self):
        transactions = self.filter(public=True)
        return transactions
    def get_transactions_for_user(self, user):
        # Filters all transactions where the user is eigther party one or two
        transactions = self.objects.filter( Q(party_1=user) | Q(party_2=user))
        return transactions
class Transaction(models.Model):
    """
    A Transaction defines the negotiation, fulfillment, merit donation and review of the transaction.
    A transaction can result from an offer or request, in which case some items and settings get copied.
    A transaction can be public (True) in which case the merit donation is applied after fulfillment for
    settings.MERIT_DONATION_PERIOD_HOURS
    """

    party_1     = models.ForeignKey(User, help_text=_("The party initiating the transaction"), related_name="first_party_to_transaction", on_delete=models.CASCADE)
    party_2     = models.ForeignKey(User, help_text=_("The other party to the transaction"), related_name="second_party_to_transaction", on_delete=models.CASCADE)

    posting     = models.ForeignKey(Posting, help_text=_("The posting the transaction is based on"),
        related_name="transactions", on_delete=models.PROTECT, null=True, blank=True)

    # The Posting the Transaction belongs to
    #limit = models.Q( app_label = 'market', model = 'posting')
    #content_type = models.ForeignKey(ContentType, limit_choices_to = limit, on_delete=models.CASCADE, null=True, blank=True)
    #object_id = models.PositiveIntegerField(null=True, blank=True)
    #content_object = GenericForeignKey('content_type', 'object_id')

    # Terms
    free_terms = GenericRelation('FreeTerm')
    # More type of terms...

    # Reverse relation from Transaction to the GenericForeignKey of DoubleEntryAccount
    money_account = GenericRelation(MoneyAccount)
    merit_account = GenericRelation(MeritAccount)

    # Flags
    status      = models.PositiveSmallIntegerField(_('Status of the Transaction'), choices=TRANSACTION_STATUS, default=1)
    public      = models.BooleanField(_("Transaction is public"), default=True)

    # Internal fields
    created_at  = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated_at  = models.DateTimeField(_('Updated at'), auto_now=True)

    objects = TransactionManager()

    def __str__(self):
        return "Transaction between %s and %s " % (self.party_1, self.party_2)

    def save(self, *args, **kwargs):
        # TODO: Check that party_1 and party_2 are not the same
        super(Transaction, self).save(*args, **kwargs)

    @property
    def name(self):
        return self.__str__()

    def is_negotiation_completed(self):
        #
        pass

    def is_transaction_fulfilled(self):
        # Checks if all transaction elements are fulfilled
        pass

    def is_in_merit_donation_period(self):
        # Checks if all transaction elements are fulfilled
        pass

    def has_two_different_parties(self):
        if self.party_1 != self.party_2:
            return True
        return False

    class Meta:
        verbose_name = _('Transaction')
        verbose_name_plural = _('Transactions')
        ordering = ['-created_at',]

#
# SIGNALS
#

# TRANSACTION CREATED (post save)
@receiver(post_save, sender=Transaction)
def transaction_created(sender, instance, **kwargs):
    """
    Creates a money and merit account for the transactions
    If transaction created from offer or request copies terms and sets attributes
    """
    if kwargs['created']:
        # Create MoneyAccount
        new_money_account = MoneyAccount.objects.create(
            content_type=ContentType.objects.get_for_model(instance),
            object_id=instance.pk,
            name = "Money Account for the %s" % (instance),
            type = 2)
        # Create MeritAccount if transaction is public
        if instance.public == True:
            new_merit_account = MeritAccount.objects.create(
                content_type=ContentType.objects.get_for_model(instance),
                object_id=instance.pk,
                name = "Merit Account for the %s" % (instance),
                type = 2)
        # Copy Terms when created from a posting:
        if instance.posting:
            # Copy the terms from posting and attach them to the transaction
            for term in instance.posting.free_terms.all():
                new_term = term
                # Set id & pk to Null for copying
                new_term.pk = None
                new_term.id = None
                new_term.content_type=ContentType.objects.get_for_model(instance)
                new_term.object_id=instance.pk
                # Set the new terms receiver and fulfiller based on the blanks and who is the owner
                # TODO: Critical Part! Rewrite so it fits all cases
                if new_term.receiver is None:
                    new_term.receiver = instance.party_2
                elif new_term.fulfiller is None:
                    new_term.fulfiller = instance.party_2
                new_term.save()
