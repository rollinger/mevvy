from django.shortcuts import render

class MarketListView(GenericListView):
    """
    Displays the view with offer and request in the area.
    Searchable and filterable
    """
    pass

class OfferDetailView(GenericDetailView):
    """
    Display one Offer
    """
    pass

class RequestDetailView(GenericDetailView):
    """
    Displays one Request View
    """
    pass

class TransactionListView(GenericListView):
    """
    Displays the list of completed transactions that are elegible for 
    Merit Donations. Searchable and filterable
    """
    pass
