# TODO:
# O add free_terms to public serializer


#from rest_framework import serializers, viewsets, mixins, routers, filters
#from rest_framework.generics import RetrieveAPIView, ListAPIView
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.serializers import ModelSerializer, HyperlinkedModelSerializer
from rest_framework.serializers import CharField, RelatedField
#from generic_relations.relations import GenericRelatedField
from rest_framework.permissions import IsAuthenticated, AllowAny

from django.contrib.auth import get_user_model

from mevvy.users.apiv1 import UserSerializer
from mevvy.market.models import Posting, FreeTerm, Transaction

User = get_user_model()

# Serializers & ViewSets for Postings, Transactions with Permission

class FreeTermObjectRelatedField(RelatedField):
    """ A custom field to use for the `free term` generic relationship."""
    def to_representation(self, value):
        """
        Serialize free terms to objects to a simple textual representation.
        see: https://www.django-rest-framework.org/api-guide/relations/#generic-relationships
        """
        if isinstance(value, Posting):
            return 'Posting: ' + value.title
        elif isinstance(value, Transaction):
            return 'Transaction: ' + value.__str__()
        raise Exception('Unexpected type of tagged object')

class FreeTermPublicMarketSerializer(HyperlinkedModelSerializer):
    receiver   = UserSerializer()
    fulfiller   = UserSerializer()
    status    = CharField(source='get_status_display')
    content_object = FreeTermObjectRelatedField(read_only='True')
    class Meta:
        model = FreeTerm
        fields = ('url', 'content_object', 'receiver', 'fulfiller', 'text', '_negotiable', 'status')

class PostingPublicMarketSerializer(HyperlinkedModelSerializer):
    owner   = UserSerializer()
    type    = CharField(source='get_type_display')
    free_terms  = FreeTermPublicMarketSerializer(many=True)
    class Meta:
        model = Posting
        fields = ('url', 'type', 'title', 'description', 'free_terms', 'published', 'owner', 'created_at', 'updated_at')

class TransactionPublicMarketSerializer(HyperlinkedModelSerializer):
    party_1   = UserSerializer()
    party_2   = UserSerializer()
    posting   = PostingPublicMarketSerializer()
    status    = CharField(source='get_status_display')
    free_terms  = FreeTermPublicMarketSerializer(many=True)
    class Meta:
        model = Transaction
        fields = ('url', 'status', 'public', 'party_1', 'party_2', 'free_terms', 'posting', 'created_at', 'updated_at')

#
# ViewSets for the general public market (LIST, GET - read only)
#
class FreeTermPublicMarketSerializer(ReadOnlyModelViewSet):
    permission_classes = [AllowAny]
    queryset = FreeTerm.objects.all()
    serializer_class = FreeTermPublicMarketSerializer

class PostingPublicMarketViewSet(ReadOnlyModelViewSet):
    permission_classes = [AllowAny]
    queryset = Posting.objects.all_published()
    serializer_class = PostingPublicMarketSerializer

class TransactionPublicMarketViewSet(ReadOnlyModelViewSet):
    permission_classes = [AllowAny]
    queryset = Transaction.objects.all_public()
    serializer_class = TransactionPublicMarketSerializer



"""
# Serializers & ViewSets for Postings with Permission
# LIST, GET, PUT, POST, PATCH
# Serializers define the API representation.
class PostingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Posting
        fields = ('id', 'type', 'title', 'description',
            'published', 'created_at', 'updated_at')
        #depth = 1

# ViewSets define the view behavior.
class PostingViewSet(mixins.RetrieveModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    queryset = Posting.objects.all()
    serializer_class = PostingSerializer
    #search_fields = ('title', 'company__name')
    #filter_backends = (filters.SearchFilter, django_filters.rest_framework.DjangoFilterBackend,)
    #filter_class = ProductPageFilter
"""
