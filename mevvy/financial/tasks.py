from django.contrib.auth import get_user_model

from config import celery_app

from .models import Move

User = get_user_model()


@celery_app.task()
def post_pending_moves():
    """ Posts all drafted moves
    """
    pending_moves = Move.objects.filter(status=1) # Drafts
    if pending_moves.exists():
        for move in pending_moves:
            move.post_move()
    return "%d moves posted." % ( pending_moves.count() )


#@celery_app.task()
def renew_merits():
    """ Renews the Merits of each active User Account.
    Should be called by a Periodic Solar Event Tasks """
    # Currently just sets each Account to 1.00 Merits
    MERITS = 2.00
    user_accounts = User.objects.filter(is_active=True)
    for user in user_accounts:
        try:
            account = user.account.get()
            account.merit_balance = MERITS
            account.save()
        except:
            pass
