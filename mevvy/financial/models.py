from decimal import Decimal
from django.utils import timezone
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.utils.translation import ugettext_lazy as _

from datetime import datetime



#
# DOUBLE ENTRY ACCOUNTING SCHEMA
# see: http://filipmatthew.com/technology/database-schema-of-double-entry-accounting-module/
# see:
ACCOUNT_TYPE = (
    #(0,_('Merit')),       #
    (1,_('Other')),       #
    (2,_('Receivable')),  #
    (3,_('Payable')),     #
    (4,_('Liquidity')),   #
)
class DoubleEntryAccountManager(models.Manager):
    pass
class DoubleEntryAccount(models.Model):
    """
    Base Class for Money and Merit Accounts
    """
    # Generic Foreign Key (limits: User, Transaction)
    limit = models.Q( app_label = 'users', model = 'user') | models.Q(app_label = 'market', model = 'transaction')
    content_type = models.ForeignKey(ContentType, limit_choices_to = limit, on_delete=models.PROTECT, null=True, blank=True)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    move_lines = GenericRelation('MoveLine')

    code    = models.CharField(_('Account-Code'), max_length=64, null=True, blank=True)
    name    = models.CharField(_("Name of the account"), default="Unnamed Account", max_length=255)
    type    = models.PositiveSmallIntegerField(_('Type of the account'), choices=ACCOUNT_TYPE, default=2)
    credit  = models.DecimalField(_('Credit balance of the account'), max_digits=12, decimal_places=4, default=0.0000)
    debit   = models.DecimalField(_('Debit balance of the account'), max_digits=12, decimal_places=4, default=0.0000)
    balance = models.DecimalField(_('Balance of the account (credit - debit)'), max_digits=12, decimal_places=4, default=0.0000)

    # Flags
    locked = models.BooleanField(_('Account is locked'), default=False)

    # Internal fields
    created_at  = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated_at  = models.DateTimeField(_('Updated at'), auto_now=True)

    objects = DoubleEntryAccountManager()

    def __str__(self):
        if self.content_object:
            return "%s %s: %0.4f" % (self.content_object, self.name, self.balance)
        else:
            return "%s %s: %0.4f" % (self.code, self.name, self.balance)

    def save(self, *args, **kwargs):
        # Auto calc balance on save
        self.balance = self.credit - self.debit
        super(DoubleEntryAccount, self).save(*args, **kwargs)

    @property
    def balanced(self):
        # Checks if debit and credit are balanced (account is closed/reconciled)
        if self.credit == self.debit:
            return True
        return False

    @property
    def is_abstract(self):
        if not self.content_object:
            return True
        return False

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "name__icontains", )

    class Meta:
        abstract = True
        verbose_name = _('Double Entry Account')
        verbose_name_plural = _('Double Entry Accounts')
        # ASC Order: Most money first
        ordering = ['-balance',]#


class MoneyAccount(DoubleEntryAccount):
    """
    INHERITED FROM DoubleEntryAccount
    The double entry account class holds the balances of the credit and debit side.
    It can be associated to a content object. An account object that is not associated
    to a content object is abstract
    """

    class Meta:
        verbose_name = _('Money Account')
        verbose_name_plural = _('Money Accounts')
        # ASC Order: Most merits first
        ordering = ['-balance',]#



class MeritAccount(DoubleEntryAccount):
    """
    INHERITED FROM DoubleEntryAccount
    The double entry merit account class holds the balances of the credit and debit side
    of merit transfers.
    """

    class Meta:
        verbose_name = _('Merit Account')
        verbose_name_plural = _('Merit Accounts')
        # ASC Order: Most merits first
        ordering = ['-balance',]#



MOVE_STATUS = (
    (1,_('Draft')),       #
    (2,_('Posted')),    #
)
class MoveManager(models.Manager):
    pass
class Move(models.Model):
    """
    A move consists of two or more move lines transfer money between two
    DoubleEntryAccounts or DoubleEntryMeritAccounts.
    A move with a line on the debit side of a Merit Account and another line to
    the credit side of a Money Account creates Merits into Money.
    """
    name        = models.CharField(_("Name of the move"), default="Unnamed Move", max_length=255)
    reference   = models.CharField(_("Reference of the move"), default="Unreferenced Move", max_length=255)
    comment     = models.CharField(_("Comment to the move"), max_length=255, null=True, blank=True)
    status      = models.PositiveSmallIntegerField(_('Status of the Move'), choices=MOVE_STATUS, default=1)
    date_posted = models.DateTimeField(_('Date the Move was posted'), null=True, blank=True)
    merit_based = models.BooleanField(_("Merit based move"), default=False)
    # Internal fields
    created_at  = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated_at  = models.DateTimeField(_('Updated at'), auto_now=True)

    objects = MoveManager()

    def __str__(self):
        return "%s %s" % (self.status, self.name)

    def save(self, *args, **kwargs):
        # Auto set merit_based on save
        if not self.lines.filter( content_type=ContentType.objects.get_for_model(MeritAccount) ):
            self.merit_based = False
        self.merit_based = True
        super(Move, self).save(*args, **kwargs)

    #
    # Main Method for posting accounting moves
    #
    def post_move(self):
        # reconciles all unreconciled move lines if move is balanced
        if self.balanced:
            for line in self.lines.all():
                if not line.reconciled:
                    line.reconcile_move_line()
            if self.reconciled:
                self.status = 2 # Move set to posted
                self.date_posted = timezone.now()
                self.save()

    def unpost_move(self):
        # TODO: Create the reverse of the Move to undo the move
        pass

    @property
    def is_merit_based(self):
        # Checks if any move line touches a merit Account
        if self.lines.filter(content_type=ContentType.objects.get_for_model(MeritAccount)).exists():
            return True
        return False

    @property
    def balanced(self):
        # Checks if all move lines are balanced between credit and debit
        balance = Decimal(0.0000)
        for line in self.lines.all():
            balance += line.credit
            balance -= line.debit
        if balance == 0.0:
            return True
        return False

    @property
    def reconciled(self):
        # Checks if all move lines are reconciled
        for line in self.lines.all():
            if not line.reconciled:
                return False
        return True

    class Meta:
        verbose_name = _('Move')
        verbose_name_plural = _('Moves')
        # ASC Order: Newest first
        ordering = ['-created_at',]



class MoveLineManager(models.Manager):
    pass
class MoveLine(models.Model):
    """
    Move Lines represent transfer of money (or merits) between credit and debit
    side of DoubleEntryAccounts or DoubleEntryMeritAccounts.
    """
    move    = models.ForeignKey(Move, help_text=_('The move the change belongs to.'),
            related_name="lines", on_delete=models.CASCADE)

    # Generic Foreign Key (limits: MoneyAccount, MeritAccount)
    #limit = models.Q( app_label = 'financial', model = 'double_entry_account') | models.Q(app_label = 'financial', model = 'double_entry_merit_account')
    limit = models.Q( app_label = 'financial', model = 'moneyaccount') | models.Q(app_label = 'financial', model = 'meritaccount')
    content_type = models.ForeignKey(ContentType, limit_choices_to = limit, on_delete=models.CASCADE, null=True, blank=True)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    credit  = models.DecimalField(_('Credit balance of the move line'), max_digits=12, decimal_places=4, default=0.000)
    debit   = models.DecimalField(_('Debit balance of the move line'), max_digits=12, decimal_places=4, default=0.000)
    # balance = models.DecimalField(_('Balance of the move line (credit - debit)'), max_digits=12, decimal_places=4, default=0.0000)
    comment     = models.CharField(_("Comment to the move line"), max_length=255, null=True, blank=True)

    # Flags
    date_posted = models.DateTimeField(_('Date the move line was posted'), null=True, blank=True)
    reconciled = models.BooleanField(_("Move line is reconciled"), default=False)

    # Internal fields
    created_at  = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated_at  = models.DateTimeField(_('Updated at'), auto_now=True)

    objects = MoveLineManager()

    def __str__(self):
        if self.credit > 0.0000:
            return "%d credit to %s" % (self.credit, self.account)
        elif self.debit > 0.0000:
            return "%d debit to %s" % (self.credit, self.account)

    @property
    def account(self):
        return self.content_object

    def reconcile_move_line(self):
        reconciled = False
        # Update Account
        if self.credit > 0.0000:
            self.account.credit += self.credit
            self.account.save()#update_fields=['credit'])
            reconciled = True
        if self.debit > 0.0000:
            self.account.debit += self.debit
            self.account.save()#update_fields=['debit'])
            reconciled = True
        # Update move line
        if reconciled == True:
            self.date_posted = timezone.now()
            self.reconciled = True
            self.save()

    class Meta:
        verbose_name = _('Move Line')
        verbose_name_plural = _('Move Lines')
        # ASC Order: Oldest first
        ordering = ['created_at',]



#
# Accounting Shortcuts
#
def create_booking_move(credit_account, debit_account, amount, name, apply=False, *args, **kwargs):
    # Create Move from one money account to another money account
    new_move = Move.objects.create(
        name=name,
        #reference=reference
        )
    credit_line = MoveLine.objects.create(
        move= new_move,
        content_type=ContentType.objects.get_for_model(credit_account),
        object_id=credit_account.pk,
        credit = amount,
        )
    debit_line = MoveLine.objects.create(
        move= new_move,
        content_type=ContentType.objects.get_for_model(debit_account),
        object_id=debit_account.pk,
        debit = amount,
        )
    if apply:
        new_move.post_move()
    return new_move

def transforming_merits_to_money(user):
    credit_account = user.money_account.get()
    debit_account = user.merit_account.get()
    amount = debit_account.balance
    name = "Creating %d merits into %d money for %s" % (amount,amount,user)
    if amount > 0.0:
        create_booking_move(credit_account, debit_account, amount, name, apply=False)
