# Generated by Django 2.2.3 on 2019-08-11 18:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('financial', '0024_move_merit_based'),
    ]

    operations = [
        migrations.AlterField(
            model_name='move',
            name='merit_based',
            field=models.BooleanField(default=False, verbose_name='Merit based move'),
        ),
    ]
