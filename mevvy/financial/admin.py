from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline
from django.contrib.contenttypes.models import ContentType
from .tasks import renew_merits
from .models import MoneyAccount, MeritAccount, Move, MoveLine

class MoveLineReadonlyAccountInline(GenericTabularInline):
    model = MoveLine
    extra = 0
    readonly_fields = ('move', 'credit', 'debit', 'date_posted','reconciled',)
    def has_add_permission(self, request):
        return False
    def has_delete_permission(self, request, obj=None):
        # No one can delete Moves
        return False
@admin.register(MoneyAccount)
class MoneyAccountAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('__str__', 'content_object', 'name', 'type', 'credit', 'debit', 'balance', 'balanced')
    list_display_links = ('__str__',)
    list_filter = ('type',)
    readonly_fields = ['created_at', 'updated_at', 'balance'] # 'credit', 'debit',
    search_fields = ["name",]
    inlines = [MoveLineReadonlyAccountInline,]
    autocomplete_lookup_fields = {
        'generic': [['content_type', 'object_id'],],
    }
    #actions = ['',]
@admin.register(MeritAccount)
class MeritAccount(admin.ModelAdmin):
    save_on_top = True
    list_display = ('__str__', 'content_object', 'name', 'type', 'credit', 'debit', 'balance', 'balanced')
    list_display_links = ('__str__',)
    list_filter = ('type',)
    readonly_fields = ['created_at', 'updated_at', 'balance'] # 'credit', 'debit',
    search_fields = ["name",]
    inlines = [MoveLineReadonlyAccountInline,]
    autocomplete_lookup_fields = {
        'generic': [['content_type', 'object_id'],],
    }
    #actions = ['',]



class MoveLineInline(admin.TabularInline):
    model = MoveLine
    fk_name = 'move'
    extra = 2
    readonly_fields = ('date_posted','reconciled',)
    autocomplete_lookup_fields = {
        'generic': [['content_type', 'object_id'],],
    }
@admin.register(Move)
class MoveAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('name', 'merit_based', 'balanced', 'reconciled', 'status', 'date_posted',)
    list_display_links = ('name',)
    list_filter = ('status','merit_based')
    search_fields = ["name","reference","comment"]
    readonly_fields = ['created_at', 'updated_at', 'date_posted', 'status',]
    inlines = [MoveLineInline,]
    actions = ['post_move',]
    def has_delete_permission(self, request, obj=None):
        # No one can delete Moves
        return True# False
    def post_move(self, request, queryset):
        for move in queryset:
            move.post_move()
    post_move.short_description = "Post Double Entry Move(s)"
