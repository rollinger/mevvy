from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model

from mevvy.users.forms import UserChangeForm, UserCreationForm

User = get_user_model()


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):

    form = UserChangeForm
    add_form = UserCreationForm
    fieldsets = (("User", {"fields": ("name",)}),) + auth_admin.UserAdmin.fieldsets
    list_display = ["pk", "username", "name", "is_superuser","is_active"]
    list_display_links = ('username',)
    search_fields = ["name"]
    actions = ['redeem_merits',]
    def redeem_merits(self, request, queryset):
        for user in queryset:
            user.redeem_merits()
    redeem_merits.short_description = "Redeem Merits"
