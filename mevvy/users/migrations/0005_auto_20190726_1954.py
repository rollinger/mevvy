# Generated by Django 2.2.3 on 2019-07-26 18:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_wallettransfer_merit_based'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wallettransfer',
            name='target',
            field=models.ForeignKey(blank=True, help_text='Target of the transfer', null=True, on_delete=django.db.models.deletion.PROTECT, related_name='incoming_transfers', to='users.Wallet'),
        ),
    ]
