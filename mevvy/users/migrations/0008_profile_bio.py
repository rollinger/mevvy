# Generated by Django 2.2.3 on 2019-08-20 09:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_auto_20190727_1719'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='bio',
            field=models.CharField(blank=True, max_length=1000, verbose_name='Short bio'),
        ),
    ]
