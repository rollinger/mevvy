from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from django.db.models.signals import post_save
from django.dispatch import receiver

from django.conf import settings

from mevvy.financial.models import MoneyAccount, MeritAccount
from mevvy.financial.models import transforming_merits_to_money

class User(AbstractUser):

    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_("Name of User"), blank=True, max_length=255)

    # Reverse relation from users to the GenericForeignKey of Accounts
    money_account = GenericRelation(MoneyAccount)
    merit_account = GenericRelation(MeritAccount)

    def pay_money_to(self, transaction, amount):
        # Pays money from the money account to a transaction account
        if money_account.balance >= amount:
            pass

    def donate_merits_to(self, transaction, amount):
        # donates merits to a transaction account
        if merit_account.balance >= amount:
            pass

    def redeem_merits(self):
        #
        transforming_merits_to_money(self)


    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "name__icontains", )

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})



class ProfileManager(models.Manager):
    pass
class Profile(models.Model):
    owner       = models.OneToOneField(User,
                help_text=_('Owner of the Profile'), related_name="profile", on_delete=models.PROTECT,)
    bio         = models.CharField(_("Short bio"), max_length=1000, blank=True)

    # TODO: Make a decent profile
    # photo
    # trade score (sum of money transfered)
    # donation score (sum of merits transfered)
    # review score

    # Internal fields
    created_at  = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated_at  = models.DateTimeField(_('Updated at'), auto_now=True)

    objects = ProfileManager()

    def __str__(self):
        return "%s's Profile" % (self.owner)

    class Meta:
        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')
        ordering = ['-updated_at',]



#
# SIGNALS
#

# USER CREATION (post save)
@receiver(post_save, sender=User)
def user_created(sender, instance, **kwargs):
    if kwargs['created']:
        # Add Money Account Instance
        new_wallet_account = MoneyAccount.objects.create(
            content_type=ContentType.objects.get_for_model(instance),
            object_id=instance.pk,
            name = _('Money Wallet'),
            # credit = 5.00 # Default Value
            )
        # Add Merit Account Instance
        new_merit_account = MeritAccount.objects.create(
            content_type=ContentType.objects.get_for_model(instance),
            object_id=instance.pk,
            name = _('Merit Wallet'),
            # credit = 5.00 # Default Value
            )
        # Make a Merit Transfer of 5.00 merits
        # TODO:

        # Add Profile Instance
        new_profile = Profile.objects.create(owner=instance)
