#from rest_framework import serializers, viewsets, mixins, routers, filters
from rest_framework.generics import RetrieveAPIView, ListAPIView
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.serializers import ModelSerializer, HyperlinkedModelSerializer
from rest_framework.serializers import CharField
from rest_framework.permissions import IsAuthenticated, AllowAny

from django.contrib.auth import get_user_model

from mevvy.users.models import Profile

User = get_user_model()

# Serializers & ViewSets for Users with Profiles Permission
# LIST, GET
# Serializers define the API representation.
class FullUserSerializer(HyperlinkedModelSerializer):
    profile_bio = CharField(read_only=False, source='profile.bio')
    class Meta:
        model = User
        fields = ('url', 'username', 'name', 'profile_bio')
class UserSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username',)
# ViewSets define the view behavior.
class UserViewSet(ReadOnlyModelViewSet):
    permission_classes = [AllowAny]
    queryset = User.objects.all()
    #serializer_class = UserSerializer
    #lookup_field = 'username'
    def get_serializer_class(self):
        # If the request is authenticated return the full serializer
        if self.request.successful_authenticator:
            return FullUserSerializer
        return UserSerializer
